public class Main {

    public static void main(String[] args) {
        Calc calc = new Calc();
        System.out.println(calc.operate("+", 33.0, 45.0));
        System.out.println(calc.operate("-", 31.0, 47.0));
        System.out.println(calc.operate("*", 30.0, 24.0));
        System.out.println(calc.operate("/", 33.0, 14.0));
        System.out.println(calc.operate("@", 33.0, 14.0));
    }
}
